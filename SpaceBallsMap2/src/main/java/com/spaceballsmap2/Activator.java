package com.spaceballsmap2;

import com.spaceballs.Ball;
import com.spaceballs.Planeta;
import com.spaceballs.Select;
import com.spaceballs.Star;
import com.spaceballsmap.Map;
import com.spaceballsmap.ISpaceBallsMap;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.Random;
import javax.swing.ImageIcon;
import maybach.engine.Engine;
import maybach.engine.IEngine;
import maybach.layer.Layer;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

@Component(name = "Map2")
public class Activator extends Map implements BundleActivator, ISpaceBallsMap {
    @Reference
    private static IEngine e = null;
    private BundleContext thisContext;
    
    public void bindE(IEngine iengine) {
        this.e = iengine;
    }

    public void unbindE(IEngine iengine) {
    }
    
    public void start(BundleContext context) throws Exception {
        thisContext = context;
        this.setImage(new ImageIcon(this.getClass().getClassLoader().getResource("images/2map.png")).getImage());
        context.registerService(Map.class.getName(), this, null);
    }

    public void stop(BundleContext context) throws Exception {
    }

    public void setMapUp() {
//        ServiceReference ref = thisContext.getServiceReference(Engine.class.getName());
//        Engine e = (Engine) thisContext.getService(ref);

        e.setBackground(Color.BLACK);

        e.RegisterLayer(new Layer("space"));
        e.RegisterLayer(new Layer("planety"));
        e.RegisterLayer(new Layer("balls"));
        e.RegisterLayer(new Layer("kule"));
        e.RegisterLayer(new Layer("www"));

        BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);

        Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "blank cursor");

        Ball kulka2 = new Ball(-100000, -100000, 10, 15);
        kulka2.setBackColor(Color.red);
        e.RegisterShape(kulka2, "balls");

        Ball kulka1 = new Ball(-300000, -300000, 10, 15);
        kulka1.setBackColor(Color.green);
        e.RegisterShape(kulka1, "balls");

        for (int i = 1; i < 10; i++) {
            Star kulka = new Star(-10, -10, 0);
            kulka.setBackColor(Color.white);
            e.RegisterShape(kulka, "space");
        }

        System.setProperty("sun.java2d.opengl", "True");

        Random r = new Random();

        Planeta slonce1 = new Planeta(1000, 100, 600, null, 1, 0);
        slonce1.setBackColor(Color.yellow);
        e.RegisterShape(slonce1, "planety");

        Planeta slonce2 = new Planeta(2500, 100, 600, null, 1, 0);
        slonce2.setBackColor(Color.yellow);
        e.RegisterShape(slonce2, "planety");

        Planeta slonce3 = new Planeta(1750, 1000, 600, null, 1, 0);
        slonce3.setBackColor(Color.yellow);
        e.RegisterShape(slonce3, "planety");

        //x, y, pROMIEN, DO OKOLA CZEGO, KTORA ORBITA, PREDKOSC
        Planeta planeta2 = new Planeta(1750, 0, 150, slonce1, 1, 0.6);
        planeta2.setBackColor(Color.RED);
        e.RegisterShape(planeta2, "planety");

        Planeta planeta = new Planeta(1750, 0, 170, slonce2, 1, 0.6);
        planeta.setBackColor(Color.BLUE);
        e.RegisterShape(planeta, "planety");

        Planeta planeta3 = new Planeta(1750, 0, 150, slonce3, 1, 0.6);
        planeta3.setBackColor(Color.GREEN);
        e.RegisterShape(planeta3, "planety");

        Select sel = new Select(-100, -100, 100);
        sel.setBackColor(Color.GRAY);
        e.addMouseListener(sel);
        e.addMouseMotionListener(sel);
        e.addMouseWheelListener(sel);
        e.RegisterShape(sel);
    }

}
