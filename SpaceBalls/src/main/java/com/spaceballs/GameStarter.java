/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spaceballs;

import com.spaceballsmap.IGameStarter;
import java.awt.Color;
import java.awt.Frame;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JPanel;
import maybach.engine.Engine;
import maybach.engine.IEngine;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.BundleException;
import org.osgi.framework.ServiceReference;
/**
 *
 * @author tkarpinski
 */
@Component(name = "GameStarter")
public class GameStarter implements IGameStarter{
    private BundleContext thisContext;
    public static JFrame frame;
    private static JPanel panel;
    public static Color playerColor = Color.BLUE;
    public static Engine e;
    private Bundle thisBundle;
    @Reference
    private static IEngine e1 = null;
    
    public void bindE1(IEngine iengine) {
        this.e = (Engine) iengine;
        this.e1 = iengine;
    }
    public void unbindE1(IEngine iengine) {
    }

    public GameStarter() {
    }
    GameStarter(BundleContext context) {
        thisContext = context;
    }
    
    public void startGame(){
        thisBundle = thisContext.getBundle();
//        ServiceReference ref = thisContext.getServiceReference(Engine.class.getName());
//        e = (Engine) thisContext.getService(ref);
        try {
        playSound pl = new playSound();
        } catch (Exception e) {
            
        }
        frame = new JFrame("SpaceBalls");
        frame.setVisible(true);
        frame.setSize(800, 600);
//        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setExtendedState(Frame.MAXIMIZED_BOTH);
        panel = new JPanel();
        panel.setLayout(null);
        panel.setBackground(Color.BLACK);

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent event) {
                e.removeAll();
                e.Stop();
                System.exit(0);
                try {
                    thisBundle.stop();
                } catch (BundleException ex) {
                    Logger.getLogger(Activator.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

        startEngine();
    }

    public static void startEngine() {
        e.setBackground(Color.BLACK);

        frame.addKeyListener(new KeyListener() {
            @Override
            public void keyPressed(KeyEvent arg0) {
                if (arg0.getKeyCode() == 27) {
                    WindowEvent wev = new WindowEvent(frame, WindowEvent.WINDOW_CLOSING);
                    Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(wev);
                }
            }

            @Override
            public void keyReleased(KeyEvent arg0) {
            }

            @Override
            public void keyTyped(KeyEvent arg0) {
            }
        });

        frame.add(e);
        frame.remove(panel);
        e.repaint();
        frame.repaint();
        frame.validate();

        e.Start();
    }
}
