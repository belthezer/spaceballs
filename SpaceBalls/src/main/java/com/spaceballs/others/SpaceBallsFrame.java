package com.spaceballs.others;

import com.spaceballs.Ball;
import com.spaceballs.Planeta;
import com.spaceballs.Select;
import com.spaceballs.Star;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;

import maybach.engine.Engine;
import maybach.layer.Layer;

public class SpaceBallsFrame extends JFrame{
    public SpaceBallsFrame() {
    super();
    Engine e = Engine.getEngine();
    e.setBackground(Color.BLACK);

    e.RegisterLayer(new Layer("space"));
    e.RegisterLayer(new Layer("planety"));
    e.RegisterLayer(new Layer("balls"));
    e.RegisterLayer(new Layer("kule"));
    e.RegisterLayer(new Layer("www"));

    BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);

    Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "blank cursor");

    this.getContentPane().setCursor(blankCursor);

    Ball kulka2 = new Ball(-10000,-10000,10, 10);
    kulka2.setBackColor(Color.red);
    e.RegisterShape(kulka2, "balls");

    Ball kulka1 = new Ball(-30000,-30000,10, 10);
    kulka1.setBackColor(Color.green);
    e.RegisterShape(kulka1, "balls");

    for (int i = 1; i < 10; i++) {
            Star kulka = new Star(-10,-10,0);
            kulka.setBackColor(Color.white);
            e.RegisterShape(kulka, "space");
    }

    System.setProperty("sun.java2d.opengl","True");

    Planeta slonce1 = new Planeta(1000,800,600, null, 1, 0);
    slonce1.setBackColor(Color.yellow);
    e.RegisterShape(slonce1, "planety");

    Planeta planeta2 = new Planeta(800,800,120, slonce1, 1, 1);
    planeta2.setBackColor(Color.green);
    e.RegisterShape(planeta2, "planety");

    Planeta planeta = new Planeta(200,200,120, slonce1, 2, 0.6);
    planeta.setBackColor(Color.red);
    e.RegisterShape(planeta, "planety");

    Planeta planeta3 = new Planeta(400,400,120, slonce1, 3, 0.5);
    planeta3.setBackColor(Color.BLUE);
    e.RegisterShape(planeta3, "planety");

    Select sel = new Select(-100,-100,100);
    sel.setBackColor(Color.GRAY);
    e.addMouseListener(sel);
    e.addMouseMotionListener(sel);
    e.addMouseWheelListener(sel);

    e.RegisterShape(sel);

    this.add(e);
    e.repaint();
    this.repaint();
    this.validate();
    e.Start();
    System.out.println("Dupal");
    }
}
