package com.spaceballs;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.util.Random;

import javax.swing.ImageIcon;

import maybach.object.Circle;

public class Star extends Circle{
	int DT = 0;
	
	
	public Star(int x, int y, float r) {
		super(x, y, r);
	}
	public void paint(Graphics g)
	{
        Graphics2D g2d = (Graphics2D) g;
        g2d.setColor(this.getColor());
        double X = super.getX() - r/2;
        double Y = super.getY() - r/2;
        
        Image image = null;
        if (this.getColor() == Color.WHITE) image = new ImageIcon("graphics/whiteBall.png").getImage();
        
        g2d.setColor(Color.WHITE);
        g2d.fillOval((int)X, (int)Y, (int)r+1, (int)r+1);
	}
	
	public void update(long dt) {
		if (this.DT > 1000) {
			Random r = new Random();
			int czyZmienic = r.nextInt(1000);
			
			
			if (czyZmienic < 1) {
                                int frameWidth = GameStarter.frame.getWidth();
                                int frameHeight = GameStarter.frame.getHeight() - 29;
				int sX = r.nextInt(frameWidth);
				int sY = r.nextInt(frameHeight);
				int sR = r.nextInt(5);
				
				this.x = sX;
				this.y = sY;
				this.r = sR;
			}
		}
		this.DT += dt;
	}
}
