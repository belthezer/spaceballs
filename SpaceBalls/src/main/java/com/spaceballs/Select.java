package com.spaceballs;

import maybach.object.Circle;
import maybach.object.Shape;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.geom.Ellipse2D;
import java.util.LinkedList;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class Select extends Circle implements MouseListener, MouseMotionListener, MouseWheelListener
{
	private boolean isPressed = false;
	private boolean lewo = false;
	private boolean prawo = false;
	private boolean gora = false;
	private boolean dol = false;
	private int lastx;
	private int lasty;
	private int lastr = 50;
	private int DT = 0;
	private boolean showPlanetPanel = false;
	private Planeta clickedPlanet = null; 
	private int mouseX = 0;
	private int mouseY = 0;
	private boolean drawWin = false;
	private LinkedList<Ball> lista = new LinkedList<Ball>(){};
        private AI redAI = new AI(Color.RED);
        private AI greenAI = new AI(Color.GREEN);
//        private AI blueAI = new AI(Color.BLUE);
	private Boolean clicked = false;
	
	@Override
	public void paint(Graphics g) {
		Graphics2D g2d = (Graphics2D) g;
                
                g2d.setColor(new Color(255,255,255));
                
                g2d.drawString("Copyleft ↄ⃝ Tadeusz Karpiński 2013. All wrongs reserved.", 10, 30);
                
		Color c = g2d.getColor();
		
		int oIle = 10;
		
		if (lewo) setAllBallsPosition(0, oIle);
		if (gora) setAllBallsPosition(1, oIle);
		if (prawo) setAllBallsPosition(2, oIle);
		if (dol) setAllBallsPosition(3, oIle);
		
		int aX = this.x - (int)(this.r);
		int aY = this.y - (int)(this.r);
		g2d.setColor(this.getColor());
		g2d.setStroke(new BasicStroke(2));
		g2d.draw(new Ellipse2D.Float(aX,aY, (float)this.r*2, (float)this.r*2));
		g2d.setColor(Color.blue);
		g2d.draw(new Ellipse2D.Float(lastx-lastr,lasty-lastr, (float)lastr*2, (float)lastr*2));
		
		if (clicked) {
			//dragowanie zaznaczanie
			if (lewo) this.x += oIle;
			if (gora) this.y += oIle;
			if (prawo) this.x -= oIle;
			if (dol) this.y -= oIle;
		}
		g2d.setColor(c);
		
		if (this.drawWin == true) {
			g2d.setColor(Color.white);
                        int x = GameStarter.frame.getWidth()/2-100;
                        int y = GameStarter.frame.getHeight()/2;
			Font font1 = new Font("Arial", Font.PLAIN, 50);
                        g2d.setFont(font1);
                        Planeta planeta = (Planeta)GameStarter.e.getLayer("planety").getShapes()[2];
                        Color cr = planeta.getColor();
                        String kolor = "";
                        if (cr == Color.GREEN) kolor = "GREEN";
                        if (cr == Color.RED) kolor = "RED";
                        if (cr == Color.BLUE) kolor = "BLUE";
			g2d.drawString(kolor+" WIN!", x, y);
		}
		if (this.showPlanetPanel) {
                        int width = GameStarter.frame.getWidth()/2-100;
			g2d.setColor(new Color(100,100,100));
			g2d.fillRect(width, 50, 200, 50);
			
			g2d.setColor(Color.GRAY);
			g2d.drawRect(width, 50, 200, 50);
			
			g2d.setColor(new Color(73,82,255));
			g2d.fillRect(width+(this.clickedPlanet.getKtoryStatekProdukuje()-1)*50, 50, 50, 50);
			
			String color = "GREEN";
			
			if (this.clickedPlanet.getColor() == Color.GREEN) color = "GREEN";
			if (this.clickedPlanet.getColor() == Color.RED) color = "RED";
			if (this.clickedPlanet.getColor() == Color.BLUE) color = "BLUE";
			
			Image image = new ImageIcon(this.getClass().getClassLoader().getResource("com/spaceballs/images/statek1"+color+".png")).getImage();
			g2d.drawImage(image, width+13, 65, 25, 20, null);
			
			image = new ImageIcon(this.getClass().getClassLoader().getResource("com/spaceballs/images/statek2"+color+".png")).getImage();
			g2d.drawImage(image, width+58, 51, 34, 48, null);
			
			image = new ImageIcon(this.getClass().getClassLoader().getResource("com/spaceballs/images/statek3"+color+".png")).getImage();
			g2d.drawImage(image, width+106, 50, 37, 50, null);
			
			image = new ImageIcon(this.getClass().getClassLoader().getResource("com/spaceballs/images/statek4"+color+".png")).getImage();
			g2d.drawImage(image, width+158, 50, 35, 50, null);
			
			g2d.setColor(Color.WHITE);
			g2d.fillOval(this.mouseX-4, this.mouseY-4, 8, 8);
			
			if ((this.mouseY > 50) && (this.mouseY < 100)) {
				int roznica = this.mouseX - width;
				g2d.setColor(new Color(100,100,100));
				if (roznica < 50) {
					g2d.fillRect(width, 100, 50, 50);
					g2d.setColor(Color.GRAY);
					g2d.drawRect(width, 100, 50, 50);
					g2d.setColor(Color.white);
					Font font = new Font("Arial", Font.PLAIN, 12);
				    g2d.setFont(font);
					g2d.drawString("Speed: 4", width, 110);
					g2d.drawString("Life:      1", width, 122);
					g2d.drawString("Shoots:1", width, 134);
					g2d.drawString("Time:    1", width, 146);
				} else if (roznica < 100) {
					g2d.fillRect(width+50, 100, 50, 50);
					g2d.setColor(Color.GRAY);
					g2d.drawRect(width+50, 100, 50, 50);
					g2d.setColor(Color.white);
					Font font = new Font("Arial", Font.PLAIN, 12);
				    g2d.setFont(font);
					g2d.drawString("Speed: 3", width+50, 110);
					g2d.drawString("Life:      3", width+50, 122);
					g2d.drawString("Shoots:2", width+50, 134);
					g2d.drawString("Time:    2", width+50, 146);
				} else if (roznica < 150) {
					g2d.fillRect(width+100, 100, 50, 50);
					g2d.setColor(Color.GRAY);
					g2d.drawRect(width+100, 100, 50, 50);
					g2d.setColor(Color.white);
					Font font = new Font("Arial", Font.PLAIN, 12);
				    g2d.setFont(font);
					g2d.drawString("Speed: 2", width+100, 110);
					g2d.drawString("Life:      5", width+100, 122);
					g2d.drawString("Shoots:3", width+100, 134);
					g2d.drawString("Time:    3", width+100, 146);
				} else if (roznica < 200) {
					g2d.fillRect(width+150, 100, 50, 50);
					g2d.setColor(Color.GRAY);
					g2d.drawRect(width+150, 100, 50, 50);
					g2d.setColor(Color.white);
					Font font = new Font("Arial", Font.PLAIN, 12);
				    g2d.setFont(font);
					g2d.drawString("Speed: 1", width+150, 110);
					g2d.drawString("Life:      8", width+150, 122);
					g2d.drawString("Shoots:4", width+150, 134);
					g2d.drawString("Time:    4", width+150, 146);
				}
			}
		} 
	}
	
	@Override
	public void update(long dt) {
		DT += dt;
		
		if (DT > 50) {
			//if (this.lastr > 15) this.lastr-=5;
			DT = 0;
		}
                redAI.update(dt);
                greenAI.update(dt);
//                blueAI.update(dt);
	}
	
	@Override
	public void mouseDragged(MouseEvent e) {
		clicked = true;
		lastx = e.getX();
		lasty = e.getY();
		
		int width = GameStarter.frame.getWidth();
		int height = GameStarter.frame.getHeight()-29;
                
		int przedzial = 50;
		
		//sprawdzenie czy przy krawedzi
		if ((e.getX() < przedzial)) lewo = true; else lewo = false;
		if ((e.getY() < przedzial)) gora = true; else gora = false;
		
		if ((e.getX() > width - przedzial)) prawo = true; else prawo = false;
		if ((e.getY() > height - przedzial)) dol = true; else dol = false;
	}
	
	@Override
	public void mouseMoved(MouseEvent e) {
		this.x = e.getX();
		this.y = e.getY();
		
		this.lastx = e.getX();
		this.lasty = e.getY();
		
                int width = GameStarter.frame.getWidth();
                int height = GameStarter.frame.getHeight() - 29;
		int przedzial = 50;
		
		//sprawdzenie czy przy krawedzi
		if ((e.getX() < przedzial)) lewo = true; else lewo = false;
		if ((e.getY() < przedzial)) gora = true; else gora = false;
		
		if ((e.getX() > width - przedzial)) prawo = true; else prawo = false;
		if ((e.getY() > height - przedzial)) dol = true; else dol = false;
		
		if (this.showPlanetPanel) {
                        width = GameStarter.frame.getWidth()/2-100;
			int x = e.getX();
			int y = e.getY();
			if ((y > 50) && (y < 100) && (x > width) && (x < width+200)) {
				this.mouseX = this.x;
				this.mouseY = this.y;
			} else {
				this.mouseX = -1000;
				this.mouseY = -1000;
			}
		} else {
			this.mouseX = -1000;
			this.mouseY = -1000;
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		switch(e.getModifiers()) {
			case InputEvent.BUTTON1_MASK: {
				lastr = 15;   
				this.showPlanetPanel = false;
				break;
			}
			case InputEvent.BUTTON2_MASK: {
				this.showPlanetPanel = false;
				break;
			}
			case InputEvent.BUTTON3_MASK: {
				if (this.showPlanetPanel) {
					if (this.showPlanetPanel == true) {
                                                int width = GameStarter.frame.getWidth()/2-100;
						int x = e.getX();
						int y = e.getY();
						if ((y > 50) && (y < 100) && (x > width) && (x < width+200)) {
							int roznica = x - width;
							if (roznica <= 50) {
								this.clickedPlanet.setKtoryStatekProdukuje(1);
							} else if (roznica <= 100) {
								this.clickedPlanet.setKtoryStatekProdukuje(2);
							} else if (roznica <= 150) {
								this.clickedPlanet.setKtoryStatekProdukuje(3);
							} else {
								this.clickedPlanet.setKtoryStatekProdukuje(4);
							}
						}
					}
				} 
				this.isPlanetThere(e.getX(), e.getY());
				break;
			}
		}
	}

	@Override
	public void mousePressed(MouseEvent e) {
		clicked = true;
		this.x = e.getX();
		this.y = e.getY();
		this.mouseDragged(e);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		switch(e.getModifiers()) {
			case InputEvent.BUTTON1_MASK: {
				this.showPlanetPanel = false;
				clicked = false;
				lastr = 15;
				
				for ( Shape s : GameStarter.e.getLayer("balls").getShapes())
				{
					if (s != null) {
						if ((s.getName() == "BALL") && (s.getColor() == GameStarter.playerColor)) {
							double R = Math.sqrt((Math.pow(s.getX() - this.x, 2))+(Math.pow(s.getY() - this.y, 2)));
							if (R <= this.r) {
								lista.add((Ball)s);
							}
						}
					}
				}
				
				this.x = e.getX();
				this.y = e.getY();
				
				int ktoraWarstwa = 1;
				int ktoryNaWarstwie = 0;
				
				for (Ball _b : lista) {
					if (ktoraWarstwa == 0) {
						_b.setDest(this.lastx, this.lasty);
						ktoraWarstwa++;
					} else {
						ktoryNaWarstwie++;
						double ile = (2*ktoraWarstwa)*Math.PI;
						double kat = (double)(2*Math.PI/ile)*ktoryNaWarstwie;
						
						double dX = (double)Math.cos(kat)*(_b.getR()+8)*ktoraWarstwa;
						double dY = (double)Math.sin(kat)*(_b.getR()+8)*ktoraWarstwa;
						
						_b.setDest((int)(this.lastx+dX), (int)(this.lasty+dY));
						if (ktoryNaWarstwie == (int) ile) {
							ktoryNaWarstwie = 0;
							ktoraWarstwa++;
						}
					}
				}
				
				if ( isPressed )
				{
					this.isPressed = false;
				}
				lista.clear();
				break;
			}
			case InputEvent.BUTTON2_MASK: {
				break;
			}
			case InputEvent.BUTTON3_MASK: {
				
				break;
			}
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		int ile = e.getWheelRotation();
		ile = -ile;
		
		for ( Shape _s : GameStarter.e.getLayer("balls").getShapes()) {
			if (_s != null) {
				int rozmiar = ((Ball)_s).getRozmiar() + ile;
				((Ball)_s).zmniejszZwieksz(rozmiar, e.getX(), e.getY());
			}
		}
		
		for ( Shape _s : GameStarter.e.getLayer("kule").getShapes()) {
			if (_s != null) {
				int rozmiar = ((Ball)_s).getRozmiar() + ile;
				((Ball)_s).zmniejszZwieksz(rozmiar, e.getX(), e.getY());
			}
		}
		
		Color firstColor = GameStarter.e.getLayer("planety").getShapes()[2].getColor();
		boolean win = true;
		
		for ( Shape _s : GameStarter.e.getLayer("planety").getShapes()) {
			int rozmiar = ((Planeta)_s).getRozmiar() + ile;
			((Planeta)_s).zmniejszZwieksz(rozmiar, e.getX(), e.getY());
			if ((firstColor != ((Planeta)_s).getColor()) && (_s.getColor() != Color.YELLOW)) {
				win = false;
			}
		}
		this.drawWin = win;
	}

	public Select(int x, int y, float r)
	{
		super(x,y,r);
		this.r = r;
		this.lastr = 15;
	}
	
	private void isPlanetThere(double aX, double aY) {
		for ( Shape s : GameStarter.e.getLayer("planety").getShapes()) {
			double dist = s.distance(aX, aY);
			
			if ((dist <= ((Ball)s).getR()) && (s.getColor() != Color.YELLOW) && (s.getColor() == GameStarter.playerColor)) {
				this.showPlanetPanel = true;
				this.clickedPlanet = (Planeta)s;
			}
		}
	}
	
	private void setAllBallsPosition(int a, int oIle) {
		for ( Shape s : GameStarter.e.getLayer("balls").getShapes())
		{
			if (s != null) {
				if (s.getName().equals("BALL")) {
					Ball ball = (Ball) s;
					
					double posX = ball.getX();
					double posY = ball.getY();
					
					int destX = ball.getDestX();
					int destY = ball.getDestY();
					
					if (a == 0) {
						posX+=oIle;
						destX+=oIle;
					}
					if (a == 1) {
						posY+=oIle;
						destY+=oIle;
					}
					if (a == 2) {
						posX-=oIle;
						destX-=oIle;
					}
					if (a == 3) {
						posY-=oIle;
						destY-=oIle;
					}
					
					ball.setX((int)posX);
					ball.setY((int)posY);
					
					ball.setDest(destX, destY);
				} 
			}
		}
		
		for ( Shape s : GameStarter.e.getLayer("planety").getShapes())
		{
			if (s.getName().equals("PLANETA")) {
				Planeta planeta = (Planeta) s;
				
				double posX = planeta.getX();
				double posY = planeta.getY();
				
				int destX = planeta.getDestX();
				int destY = planeta.getDestY();
				
				if (a == 0) {
					posX+=oIle;
					destX+=oIle;
				}
				if (a == 1) {
					posY+=oIle;
					destY+=oIle;
				}
				if (a == 2) {
					posX-=oIle;
					destX-=oIle;
				}
				if (a == 3) {
					posY-=oIle;
					destY-=oIle;
				}
				
				planeta.setX((int)posX);
				planeta.setY((int)posY);
				
				planeta.setDest(destX, destY);
			}
		}
		for ( Shape s : GameStarter.e.getLayer("kule").getShapes())
		{
			if ((s != null) && (s.getName().equals("KULA"))) {
				kula aKula = (kula) s;
				
				double posX = aKula.getX();
				double posY = aKula.getY();
				
				if (a == 0) {
					posX+=oIle;
				}
				if (a == 1) {
					posY+=oIle;
				}
				if (a == 2) {
					posX-=oIle;
				}
				if (a == 3) {
					posY-=oIle;
				}
				
				aKula.setX((int)posX);
				aKula.setY((int)posY);
			}
		}
	}
}
