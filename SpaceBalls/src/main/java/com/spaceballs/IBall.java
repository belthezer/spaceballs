package com.spaceballs;

import java.awt.Color;
import java.awt.Graphics;

public interface IBall {

    double calculateAngle(double aX1, double aY1, double aX2, double aY2);

    void delete();

    double getAngle();

    int getDestX();

    int getDestY();

    int getNumerStartku();

    double getR();

    int getRozmiar();

    double getSpeed();

    int getZycie();

    double liczKatMiedzyPilkami(Ball b);

    void paint(Graphics g);

    void setAngle(double aAngle);

    void setBackColor(Color aColor);

    void setDest(int aX, int aY);

    void setNumerStartku(int aNumer);

    void setR(int aR);

    void setRozmiar(int Rozmiar);

    void setSpeed(double aSpeed);

    void setZycie(int aZycie);

    void update(long dt);

    void zmniejszZwieksz(int aRozmiar, int aX, int aY);
    
    void setX(double aX);
    
    void setY(double aY);
    
    double getX();
    
    double getY();
}
