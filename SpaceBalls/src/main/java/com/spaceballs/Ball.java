package com.spaceballs;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;

import javax.swing.ImageIcon;

import maybach.object.Circle;
import maybach.object.Shape;

public class Ball extends Circle implements IBall{
	private double speed = 20;
	private int time = 0;
	private int DT = 0;
	private double tempx = 0;//roznica zamiany double na int
	private double tempy = 0;
	private int lastx = 0; //poprzednie przesuniecie
	private int lasty = 0;
	private int magneticr = 20;
	private int rozmiar = 10;
	private int lastDeltaX = 0;
	private int lastDeltaY = 0;
	public boolean delete = false;
	protected Image image = null;
	private Color drawColor = null;
	private int numerStatku = 1;
	private int zycie = 1;
	
	public Ball(int aX, int aY, int aR, double aSpeed) {
		super(aX, aY, aR);
		this.name = "BALL";
		angle = 0;
		this.speed = aSpeed;
		
	}
	
    @Override
	public double getR() {
		return this.r;
	}
	
	@Override
	public void setAngle(double aAngle) {
		this.angle = aAngle;
	}
	@Override
	public double getAngle() {
		return this.angle;
	}
	
    @Override
	public void update(long dt) {
		time+=dt;
		DT+=dt;
		if (dt <= 0) {
			return;
		}
		if (time > 40) {
			if (this.delete) GameStarter.e.UnregisterShape(this); //zniszczenie tego Ball
			if ((this.x != this.destx) || (this.y != this.desty))	{
				double kat = this.calculateAngle(this.destx, this.desty, this.x, this.y);
				this.setAngle(kat);
				
				double dX = (double)Math.cos(this.angle);
				double dY = (double)Math.sin(this.angle);
				
				dX = (dX)*this.speed + tempx;
				dY = (dY)*this.speed + tempy;

				this.setX(((int)(this.getX()+dX)));
				this.setY(((int)(this.getY()+dY)));
				
				if ((this.lastx != 0) && (this.lasty != 0)) {
					if (((this.lastx < 0) && (dX > 0)) || ((this.lastx > 0) && (dX <0)) && ((this.lasty < 0) && (dY > 0)) || ((this.lasty > 0) && (dY <0))) {
						this.setX(this.destx);
						this.setY(this.desty);
						this.lastx = 0;
						this.lasty = 0;
					}
				}
				
				this.lastx = (int)dX;
				this.lasty = (int)dY;
				
				tempx = dX - (int)dX;
				tempy = dY - (int)dY;
			}
			time = 0;
		}
		if (DT > 500) {
			boolean stworzyl = false; //jezeli zrobi kulke to niech wypada
			int ileStworzyl = 0;
			for ( Shape s : GameStarter.e.getLayer("balls").getShapes()) {
				if ((s != null) && (stworzyl == false))
				if (s.getColor() != this.getColor()) {
					double dist = this.distance(s.getX(), s.getY());
					
					if (dist < this.getR()*6+5) {//odnosi sie do kulki TWORZENIE KUL
						
						double kat = this.calculateAngle(s.getX(), s.getY(), this.x, this.y);
						this.setAngle(kat);
						
						ileStworzyl++;
						kula kulka = new kula((int)this.getX(),(int)this.getY(),1, 1);
						kulka.setBackColor(this.getColor());
						kulka.setRozmiar(this.getRozmiar());
						kulka.setSpeed(this.getSpeed()-2);
						kulka.setAngle(kat);
						GameStarter.e.RegisterShape(kulka, "kule");
						
						if (ileStworzyl >= this.numerStatku) stworzyl = true;
					}
				} 
			}
			DT = 0;
		}
	}
	@Override
	public void paint(Graphics g) {
                int frameWidth = GameStarter.frame.getWidth();
                int frameHeight = GameStarter.frame.getHeight() - 29;
                
		if ((this.getX() > 0) && (this.getY() > 0) && (this.getX() < frameWidth) && (this.getY() < frameHeight)) {
	        Graphics2D g2d = (Graphics2D) g;
	        g2d.setColor(this.drawColor);
	        double X = super.getX() - r/2;
	        double Y = super.getY() - r/2;
	        
	        AffineTransform xform = new AffineTransform();
	        if ((this.getColor() != Color.WHITE) && (this.rozmiar > 6)) { //narysuj obrocony obrazek
	        	double scale = this.getR()/40;
	        	xform.translate(X, Y);
	        	xform.scale(scale, scale);
	        	xform.rotate(this.angle+Math.toRadians(90),20,20);
	        	g2d.drawImage(image, xform, null);
                        
	        } else { //a jak za male to piksel zwykly
	        	g2d.fillOval((int)X, (int)Y, (int)r, (int)r);
	        }
		}
	}
    @Override
	public void setDest(int aX, int aY) {
		this.destx = aX;
		this.desty = aY;
		this.tempx = 0;
		this.tempy = 0;
		this.lastx = 0;
		this.lasty = 0;
	}
	
    @Override
	public void delete() {
		GameStarter.e.UnregisterShape(this);
	}
	
    @Override
	public void zmniejszZwieksz(int aRozmiar, int aX, int aY) {
		if (aRozmiar > 2 && aRozmiar < 23) {
                        int frameWidth = GameStarter.frame.getWidth();
                        int frameHeight = GameStarter.frame.getHeight() - 29;
                        
			int width = (frameWidth)/21;// + przesuniecieX;
			int height = (frameHeight-29)/21;// + przesuniecieY;
			
			if (aRozmiar - this.rozmiar > 0) {//przybliżenie
				this.setX((int)(1.1111*(this.x-width)));
				this.setY((int)(1.1111*(this.y-height)));
				this.destx = (int)(1.1111*(this.destx-width));
				this.desty = (int)(1.1111*(this.desty-height));
				
				this.speed = (int)Math.ceil(1.1111*(this.speed));
				this.r = (int)Math.ceil(1.1111*(this.r));
				this.magneticr = (int)(1.1111*(this.magneticr));
			} else {//tutaj daje na sztywno 1,111 oddalenie
				this.x = (int)(0.9*(this.x)+width);
				this.y = (int)(0.9*(this.y)+height);
				this.destx = (int)(0.9*(this.destx)+width);
				this.desty = (int)(0.9*(this.desty)+height);
				
				this.speed = (int)(0.9*(this.speed));
				this.r = (int)(0.9*(this.r));
				this.magneticr = (int)(0.9*(this.magneticr));
			}
			this.rozmiar=aRozmiar;
		}
	}
	
    @Override
	public int getRozmiar() {
		return this.rozmiar;
	}
	
    @Override
	public void setRozmiar(int Rozmiar) {
		this.rozmiar = Rozmiar;
	}
	
    @Override
	public double liczKatMiedzyPilkami(Ball b) {
		double kat = 0;
		int deltaX = (int)(b.getX() - this.x);
		int deltaY = (int)(b.getY() - this.y);
		if (deltaX == 0) deltaX = 1;
		if (deltaY == 0) deltaY = 1;
		
		double iloraz = (double)deltaY / deltaX;
		kat = (double) Math.atan(iloraz)*(180/Math.PI);
		
		if ((deltaX < 0) && (deltaY > 0)) {
			kat = 180 + kat;
		}
		if ((deltaX < 0) && (deltaY < 0)) {
			kat = 180 + kat;
		}
		if ((deltaX > 0) && (deltaY < 0)) {
			kat = 360 + kat;
		}
			
		kat = kat*Math.PI/180;
		
		return kat;
	}
	
    @Override
	public int getDestX() {
		return this.destx;
	}
	
    @Override
	public int getDestY() {
		return this.desty;
	}
    @Override
	public void setR(int aR) {
		this.r = aR;
	}
	
    @Override
	public void setBackColor(Color aColor) {
		super.setBackColor(aColor);
		
		int numer = this.numerStatku;
		
		if (this.getColor() == Color.RED) {
			image = new ImageIcon(this.getClass().getClassLoader().getResource("com/spaceballs/images/statek"+numer+"RED.png")).getImage();
			this.drawColor = new Color(124, 2, 2);
		}
        if (this.getColor() == Color.GREEN) {
        	image = new ImageIcon(this.getClass().getClassLoader().getResource("com/spaceballs/images/statek"+numer+"GREEN.png")).getImage();
        	this.drawColor = new Color(65, 99, 0);
        }
        if (this.getColor() == Color.WHITE) {
        	image = new ImageIcon(this.getClass().getClassLoader().getResource("com/spaceballs/images/statek/whiteBall.png")).getImage();
        }
        if (this.getColor() == Color.BLUE) {
        	image = new ImageIcon(this.getClass().getClassLoader().getResource("com/spaceballs/images/statek"+numer+"BLUE.png")).getImage();
        	this.drawColor = new Color(0, 30, 128);
        }
	}
    @Override
	public double getSpeed() {
		return this.speed;
	}
	
    @Override
	public void setSpeed(double aSpeed) {
		this.speed = aSpeed;
	}
    @Override
	public double calculateAngle(double aX1, double aY1, double aX2, double aY2) {
		int deltaX = (int)(aX1 - aX2);
		int deltaY = (int)(aY1 - aY2);
		if (deltaX == 0) deltaX = 1;
		if (deltaY == 0) deltaY = 1;
		double kat = 0;
		
		double iloraz = (double)deltaY / deltaX;
		kat = (double) Math.atan(iloraz)*(180/Math.PI);
		
		if ((deltaX == 0) && (this.lastDeltaX < 0)) {
			deltaY = 1;
		} 
		if ((deltaX == 0) && (this.lastDeltaX > 0)) {
			deltaY = -1;
		}
		
		if ((deltaY == 0) && (this.lastDeltaY < 0)) {
			deltaY = 1;
		} 
		if ((deltaY == 0) && (this.lastDeltaY > 0)) {
			deltaY = -1;
		}
		
		if ((deltaX < 0) && (deltaY > 0)) {
			kat = 180 + kat;
		}
		if ((deltaX < 0) && (deltaY < 0)) {
			kat = 180 + kat;
		}
		if ((deltaX > 0) && (deltaY < 0)) {
			kat = 360 + kat;
		}
		
		kat = kat*Math.PI/180;
		
		this.lastDeltaX = deltaX;
		this.lastDeltaY = deltaY;
		
		return(kat);
	}
	
    @Override
	public void setNumerStartku(int aNumer) {
		this.numerStatku = aNumer;
		
		if (aNumer == 1) this.zycie = 1;
		if (aNumer == 2) this.zycie = 3;
		if (aNumer == 3) this.zycie = 5;
		if (aNumer == 4) this.zycie = 8;
	}
	
    @Override
	public int getNumerStartku() {
		return this.numerStatku;
	}
	
    @Override
	public void setZycie(int aZycie) {
		this.zycie = aZycie;
	}
	
    @Override
	public int getZycie() {
		return this.zycie;
	}

    public void setX(double aX) {
        this.setX(aX);
    }

    public void setY(double aY) {
        this.setY(aY);
    }
}