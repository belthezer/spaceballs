package com.spaceballs;

import com.spaceballsmap.IGameStarter;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {
    private BundleContext thisContext;
    
    public void start(BundleContext context) throws Exception{
        GameStarter gS = new GameStarter(context);
        context.registerService(IGameStarter.class.getName(), gS, null);
        thisContext = context;
    }

    public void stop(BundleContext context) throws Exception {
//        thisBundle.uninstall();
    }
}
