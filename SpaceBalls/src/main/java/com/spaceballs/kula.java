package com.spaceballs;

import java.awt.Graphics;
import java.awt.Graphics2D;

import maybach.object.Shape;

public class kula extends Ball {
	private int zasieg = 0;
	private int time = 0;
	private double roznicaX = 0;
	private double roznicaY = 0;
	
	public kula(int aX, int aY, int aR, int aSpeed) {
		super(aX, aY, aR, aSpeed);
		this.name = "KULA";
	}
	
	@Override
	public void update(long dt) {
		time += dt;
		if (time > 40) {
			Ball jakasKulka = ((Ball) GameStarter.e.getLayer("balls").getShapes()[0]);
			if (this.zasieg <= jakasKulka.getR()*6) {//ball
				for ( Shape s : GameStarter.e.getLayer("balls").getShapes()) {
					if ((s!=null) && (this.getColor() != s.getColor())) {
						double dist = this.distance(s.getX(), s.getY(), this.getX(), this.getY());
						if (dist <= ((Ball)s).getR()+5) {
							int sZycie = ((Ball)s).getZycie();
							sZycie--;
							if (sZycie <= 0) {
								GameStarter.e.UnregisterShape(s);
							} else {
								((Ball)s).setZycie(sZycie);
							}
							GameStarter.e.UnregisterShape(this);
						}
					} 
				}
				double dX = (double)Math.cos(this.getAngle());
				double dY = (double)Math.sin(this.getAngle());
				
				dX *= this.getSpeed()+this.roznicaX;
				dY *= this.getSpeed()+this.roznicaY;
				
				int idX = (int) dX;
				int idY = (int) dY;
				
				this.roznicaX = dX - idX;
				this.roznicaY = dY - idY;
				
				dX = this.getX() + dX;
				dY = this.getY() + dY;
				
				this.setX((int)(dX));
				this.setY((int)(dY));
				
				this.zasieg+=this.getSpeed();
			} else {
				GameStarter.e.UnregisterShape(this);
			}
			time = 0;
		}
	}
	@Override
	public void paint(Graphics g)
	{
		Ball jakasKulka = ((Ball) GameStarter.e.getLayer("balls").getShapes()[0]);
		if (jakasKulka.getRozmiar() > 10) {
                        int frameWidth = GameStarter.frame.getWidth();
                        int frameHeight = GameStarter.frame.getHeight() - 29;
                        
			if ((this.getX() > 0) && (this.getY() > 0) && (this.getX() < frameWidth) && (this.getY() < frameHeight)) {
		        Graphics2D g2d = (Graphics2D) g;
		        g2d.setColor(this.getColor());
		        double X = this.getX() - this.getR()/2;
		        double Y = this.getY() - this.getR()/2;
		        
	        	g2d.fillOval((int)X, (int)Y, (int)3, (int)3);
			}
		}
	}
}
