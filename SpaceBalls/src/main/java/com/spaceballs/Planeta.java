package com.spaceballs;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.AffineTransform;
import java.util.Random;

import javax.swing.ImageIcon;

import maybach.object.Shape;

public class Planeta extends Ball{
	//private double angle = 0;;
	//protected double speed = 20;
	protected int generalSpeed = 20;
	private int time = 0;
	private int level = 1;
	private int uszkodzenie = 100;
	private int ileZrobione = 0;
	private int dzielnik = 10;
	private int aTime = 0;
	private int aTime2 = 0;
	private Image image = null;
	private int generalR = 0;
	private Planeta doOkolaCzego;
	private int ktoryNaOrbcie = 0;
	private int ktoryStatekProdukuje = 1;
	
	private double lastDeltaX = 0;
	private double lastDeltaY = 0;
	
	
	public Planeta(int aX, int aY, int aR, Planeta aPlaneta, int aOrbita, double aPredkosc) {
		super(aX, aY, aR, 20);
		this.setSpeed(((aPredkosc)));
		this.name = "PLANETA";
		this.setAngle(0);
		this.generalR = aR;
		this.doOkolaCzego = aPlaneta;
		if (aPlaneta == null) this.doOkolaCzego = this;
		this.ktoryNaOrbcie = aOrbita;
	}
	@Override
	public void update(long dt) {
		time+=dt;
		aTime+=dt;
		aTime2+=dt;
		
		if (aTime2 > 1500) {
			if (this.uszkodzenie < 100) {
				this.uszkodzenie++;
			}
			aTime2 = 0;
		}
		
		if (aTime > 30) {
			this.ileZrobione += 30;
			
			if (this.ileZrobione > 1000*this.ktoryStatekProdukuje) { //tworzenie nowych statkow
				if (this.getColor() != Color.YELLOW) {
					this.ileZrobione = 0;
					Random r = new Random();
					int x = (int) ((int) this.getX()+r.nextInt((int) ((int)this.getR()/1.5))-this.getR()/3);
					int y = (int) ((int) this.getY()+r.nextInt((int) ((int)this.getR()/1.5))-this.getR()/3);
					Ball jakasKulka = ((Ball) GameStarter.e.getLayer("balls").getShapes()[0]);
					double speed = jakasKulka.getSpeed();
					Ball kulka = new Ball((x),y,(int)jakasKulka.getR(), speed+(speed/2)*(5-this.ktoryStatekProdukuje));
					kulka.setRozmiar(jakasKulka.getRozmiar());
					kulka.setNumerStartku(this.ktoryStatekProdukuje);
					kulka.setBackColor(this.getColor());
                                        GameStarter.e.RegisterShape(kulka, "balls");
					time = 0;
				}
			}
			
			//obrot planety w okol gwiazdy
			double kat = this.calculateAngle(this.getDoOkolaCzego().getX(), this.getDoOkolaCzego().getY(), this.getX(), this.getY());
			kat = Math.toDegrees(kat);
			this.setAngle(Math.toRadians(kat+30));//nie wiem po co, ale trzeba obrocic o 30 stopni
			
			kat = kat + this.getSpeed();
			kat = kat*Math.PI/180;
			
			double dX = (double)Math.cos(kat);//liczenie przesuniecia
			double dY = (double)Math.sin(kat);
			
			dX = (dX)*(this.getDoOkolaCzego().getR()/2+this.getR()*this.ktoryNaOrbcie*2);//cos sie dzieje
			dY = (dY)*(this.getDoOkolaCzego().getR()/2+this.getR()*this.ktoryNaOrbcie*2);

			dX = this.getDoOkolaCzego().getX() - dX; //dostosowuje nowa pozycje relatywnie do slonca
			dY = this.getDoOkolaCzego().getY() - dY;
			
			//jezeli slonce
			if (this.getDoOkolaCzego() == this) {
				dX = this.getX();
				dY = this.getY();
			}

			//jezeli kulka w sloncu, wrogiej planecie i podarzanie za wlasna planeta
			for ( Shape s : GameStarter.e.getLayer("balls").getShapes()) {
				if ( s != null ) {
					double dist = this.distance(s.getX(), s.getY());
					//rozbicie o wroga planete lub slonce
					if ((dist < this.getR()/2) && (this.getColor() != s.getColor())) {
						((Ball)s).delete = true;
						if (this.getColor() != Color.YELLOW) this.uszkodzenie -= ((Ball)s).getNumerStartku(); //Slonca nie da sie zniszczyc
						if ((this.uszkodzenie <= 0)) { //jezeli uszkodzenie jest juz za duze to przejmij planete
							this.setBackColor(s.getColor());
						}//jezeli statek w planecie przyjaznej, i nie przemieszcza sie to zostanie na jego orbicie
					} else if((dist < this.getR()/2) && (this.getColor() == s.getColor())) {
						if ((((Ball)s).getDestX() == (int)s.getX()) && (((Ball)s).getDestY() == (int)s.getY())) {
							
							int roznicaSTX = (int)(s.getX() - this.getX());
							int roznicaSTY = (int)(s.getY() - this.getY());
							
							s.setX((int)dX+roznicaSTX); //umieszcza i przemieszcza w oberbie dobrej planety
							s.setY((int)dY+roznicaSTY);
							((Ball)s).setDest((int)dX+roznicaSTX, (int)dY+roznicaSTY);
						}
					} else { //jeżeli nie ma naszej planety, ani wrogiej. To może ich przeznaczeniem jest być w tej planecie? Czyli inaczej darzenie do planety
						dist = Shape.distance(((Ball)s).getDestX(), ((Ball)s).getDestY(), this.getX(), this.getY());
						
						if ((dist < this.getR()/2)) { //czyli kulka leci do tej planety
							((Ball)s).setDest((int)dX, (int)dY);
						}
					}
				}
			}
//			if (this.getColor() != Color.YELLOW) {
				this.setX((int) dX);
				this.setY((int) dY);
//			}
			
			aTime = 0;
			}
		}
	
	@Override
	public void paint(Graphics g) {
                int frameWidth = GameStarter.frame.getWidth();
                int frameHeight = GameStarter.frame.getHeight() - 29;
                
		int r = (int) this.getR();
		if ((this.getX()+r > 0) && (this.getY()+r > 0) && (this.getX()-r< frameWidth) && (this.getY()-r < frameHeight)) {
	        Graphics2D g2d = (Graphics2D) g;
	        g2d.setColor(this.getColor());
	        double X = super.getX() - this.getR()/2;
	        double Y = super.getY() - this.getR()/2;
	        
	        if (this.getColor() == Color.RED) image = new ImageIcon(this.getClass().getClassLoader().getResource("com/spaceballs/images/redPlanet.png")).getImage();
	        if (this.getColor() == Color.GREEN) image = new ImageIcon(this.getClass().getClassLoader().getResource("com/spaceballs/images/greenPlanet.png")).getImage();
	        if (this.getColor() == Color.WHITE) image = new ImageIcon(this.getClass().getClassLoader().getResource("com/spaceballs/images/whiteBall.png")).getImage();
	        if (this.getColor() == Color.YELLOW) image = new ImageIcon(this.getClass().getClassLoader().getResource("com/spaceballs/images/yellowPlanet.png")).getImage();
	        if (this.getColor() == Color.BLUE) image = new ImageIcon(this.getClass().getClassLoader().getResource("com/spaceballs/images/bluePlanet.png")).getImage();
	        
	        AffineTransform xform = new AffineTransform();
	        if (this.getColor() != Color.YELLOW) {
	        	double scale = this.getR()/300;
	        	xform.translate(X, Y);
	        	xform.scale(scale, scale);
	        	xform.rotate(this.getAngle(),150,150);
	        	g2d.drawImage(image, xform, null);
	        } else {
	        	g2d.drawImage(image,(int)X, (int)Y, (int)r+1, (int)r+1, null);
	        }
	        
	        if (this.getColor() != Color.YELLOW) {
		        g2d.setColor(Color.GREEN);
		        g2d.fillRect((int)this.getX()-15, (int)(this.getY()+this.getR()/2-3), this.ileZrobione/(30*this.ktoryStatekProdukuje), 2); 
		        g2d.setColor(Color.RED);
		        g2d.fillRect((int)this.getX()-15, (int)(this.getY()+this.getR()/2+7), this.uszkodzenie/3, 2); 
	        }
		}
	}
	@Override
	public void zmniejszZwieksz(int aRozmiar, int aX, int aY) {
		double tempSpeed = this.getSpeed();
		super.zmniejszZwieksz(aRozmiar, aX, aY);
		this.setSpeed(tempSpeed);
	}
	
	public void setKtoryStatekProdukuje(int aI) {
		this.ktoryStatekProdukuje = aI;
	}
	
	public int getKtoryStatekProdukuje() {
		return this.ktoryStatekProdukuje;
	}

    public Planeta getDoOkolaCzego() {
        return doOkolaCzego;
    }

    public void setDoOkolaCzego(Planeta doOkolaCzego) {
        this.doOkolaCzego = doOkolaCzego;
    }
}
