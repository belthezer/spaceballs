package com.spaceballsmenu;

import java.awt.Frame;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

public class Activator implements BundleActivator {

    public void start(final BundleContext context) throws Exception {
        MenuFrame frame = new MenuFrame("SpaceBalls", context);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setSize(800, 600);
        frame.setExtendedState(Frame.MAXIMIZED_BOTH);
    }

    public void stop(BundleContext context) throws Exception {
        // TODO add deactivation code here
    }
    
    public void showAndCreateGUI() {
        
    }
    
}
