/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spaceballsmenu;

import com.spaceballsmap.IGameStarter;
import com.spaceballsmap.ISpaceBallsMap;
import com.spaceballsmap.Map;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;
/**
 *
 * @author tkarpinski
 */
public class MenuFrame extends JFrame{
    private JFrame thisFrame;
    private BundleContext thisContext;
    
    MenuFrame(String spaceBalls, final BundleContext context) {
        super(spaceBalls);
        thisFrame = this;
        thisContext = context;
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        final SyberiadaPanel panel = new SyberiadaPanel();
        
        panel.setBackground(Color.BLACK);

        panel.setLayout(new GridLayout(0,1));
        
        SyberiadaImage logo = new SyberiadaImage("images/logo.jpg", 1f);
        SyberiadaImage start = new SyberiadaImage("images/start.png", 0.5f);
        SyberiadaImage author = new SyberiadaImage("images/author.png", 0.5f);
        SyberiadaImage exit = new SyberiadaImage("images/exit.png", 0.5f);
        
        panel.add(logo);
        panel.add(start);
        panel.add(author);
        panel.add(exit);
        
        this.add(panel);
        
        final SyberiadaPanel startPanel = new SyberiadaPanel();
        startPanel.setBackground(Color.BLACK);
        startPanel.setLayout(new GridLayout(0,1));
        
        SyberiadaImage backMap = new SyberiadaImage("images/back.png", 1f);
        
        startPanel.add(backMap);
        
        final SyberiadaPanel authorPanel = new SyberiadaPanel();
        authorPanel.setBackground(Color.BLACK);
        authorPanel.setLayout(new GridLayout(0, 1));
        
        SyberiadaImage tadeusz = new SyberiadaImage("images/tadeusz.png", 1f);
        SyberiadaImage backAuthor = new SyberiadaImage("images/back.png", 1f);
        
        authorPanel.add(tadeusz);
        authorPanel.add(backAuthor);
        
        ServiceReference[] ref1;
        try {
            ref1 = thisContext.getAllServiceReferences(Map.class.getName(), null);

            for (int i = 0; i < ref1.length; i++) {
                Object e = (Object) thisContext.getService(ref1[i]);
                if (e instanceof ISpaceBallsMap) {
                    final ISpaceBallsMap mapa = (ISpaceBallsMap) e;
                    
                    Map mp = (Map) e;
                    Image img = mp.getImage();
                    if (img == null) System.out.println("DUPAL!!!");
                    ImageIcon icon = new ImageIcon(img);
                    JLabel btn = new JLabel("");
                    btn.setIcon(icon);
                    btn.setHorizontalAlignment(SwingConstants.CENTER);
                    startPanel.add(btn);
                    
                    btn.addMouseListener(new MouseListener() {
                        public void mouseClicked(MouseEvent me) {
                            ServiceReference ref = thisContext.getServiceReference(IGameStarter.class.getName());
                            IGameStarter gS = (IGameStarter) thisContext.getService(ref);
                            gS.startGame();

                            mapa.setMapUp();
                        }
                        public void mousePressed(MouseEvent me) {
                        }
                        public void mouseReleased(MouseEvent me) {
                        }
                        public void mouseEntered(MouseEvent me) {
                        }
                        public void mouseExited(MouseEvent me) {
                        }
                    });
                }
            }
        } catch (InvalidSyntaxException ex) {
            Logger.getLogger(MenuFrame.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        start.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                thisFrame.remove(panel);
                thisFrame.add(startPanel);
                thisFrame.validate();
                thisFrame.repaint();
            }

            public void mousePressed(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }
        });
        
        exit.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                System.exit(0);
            }

            public void mousePressed(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }
        });
        
        author.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                thisFrame.remove(panel);
                thisFrame.add(authorPanel);
                thisFrame.validate();
                thisFrame.repaint();
            }

            public void mousePressed(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }
        });
        
        backAuthor.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                thisFrame.remove(authorPanel);
                thisFrame.add(panel);
                thisFrame.validate();
                thisFrame.repaint();
            }

            public void mousePressed(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }
        });
        
        backMap.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                thisFrame.remove(startPanel);
                thisFrame.add(panel);
                thisFrame.validate();
                thisFrame.repaint();
            }

            public void mousePressed(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }
        });
    }
    
    
    
}
