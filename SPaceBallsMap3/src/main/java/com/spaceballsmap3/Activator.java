package com.spaceballsmap3;

import com.spaceballs.Ball;
import com.spaceballs.Planeta;
import com.spaceballs.Select;
import com.spaceballs.Star;
import com.spaceballsmap.Map;
import com.spaceballsmap.ISpaceBallsMap;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.util.Random;
import javax.swing.ImageIcon;
import maybach.engine.Engine;
import maybach.engine.IEngine;
import maybach.layer.Layer;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Reference;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

@Component(name = "Map3")
public class Activator extends Map implements BundleActivator, ISpaceBallsMap {
    @Reference
    private static IEngine e = null;
    private BundleContext thisContext;
    
    public void bindE(IEngine iengine) {
        this.e = iengine;
    }
    public void unbindE(IEngine iengine) {
    }
    
    public void start(BundleContext context) throws Exception {
        thisContext = context;
        this.setImage(new ImageIcon(this.getClass().getClassLoader().getResource("images/3map.png")).getImage());
        context.registerService(Map.class.getName(), this, null);
    }

    public void stop(BundleContext context) throws Exception {
    }

    public void setMapUp() {
//        ServiceReference ref = thisContext.getServiceReference(Engine.class.getName());
//        Engine e = (Engine) thisContext.getService(ref);

        this.e.setBackground(Color.BLACK);

        e.RegisterLayer(new Layer("space"));
        e.RegisterLayer(new Layer("planety"));
        e.RegisterLayer(new Layer("balls"));
        e.RegisterLayer(new Layer("kule"));
        e.RegisterLayer(new Layer("www"));

        BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);

        Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImg, new Point(0, 0), "blank cursor");

        Ball kulka2 = new Ball(-100000, -100000, 10, 15);
        kulka2.setBackColor(Color.red);
        e.RegisterShape(kulka2, "balls");

        Ball kulka1 = new Ball(-300000, -300000, 10, 15);
        kulka1.setBackColor(Color.green);
        e.RegisterShape(kulka1, "balls");

        for (int i = 1; i < 10; i++) {
            Star kulka = new Star(-10, -10, 0);
            kulka.setBackColor(Color.white);
            e.RegisterShape(kulka, "space");
        }

        System.setProperty("sun.java2d.opengl", "True");

        Random r = new Random();

        //x, y, pROMIEN, DO OKOLA CZEGO, KTORA ORBITA, PREDKOSC
        Planeta slonce1 = new Planeta(200, 200, 250, null, 3, 1.4);
        slonce1.setBackColor(Color.YELLOW);
        e.RegisterShape(slonce1, "planety");

        Planeta planeta = new Planeta(1750, 0, 170, null, 3, -0.8);
        planeta.setBackColor(Color.BLUE);
        e.RegisterShape(planeta, "planety");

        Planeta planeta2 = new Planeta(1750, 0, 150, planeta, 1, 0.8);
        planeta2.setBackColor(Color.RED);
        e.RegisterShape(planeta2, "planety");


        Planeta planeta3 = new Planeta(1750, 0, 150, planeta, 2, -0.9);
        planeta3.setBackColor(Color.GREEN);
        e.RegisterShape(planeta3, "planety");

        slonce1.setDoOkolaCzego(planeta);
//        Planeta planeta4 = new Planeta(1200, 1200, 150, null, 1, 0.6);
//        planeta4.setBackColor(Color.BLUE);
//        e.RegisterShape(planeta4, "planety");

        Select sel = new Select(-100, -100, 100);
        sel.setBackColor(Color.GRAY);
        e.addMouseListener(sel);
        e.addMouseMotionListener(sel);
        e.addMouseWheelListener(sel);
        e.RegisterShape(sel);
    }

}
