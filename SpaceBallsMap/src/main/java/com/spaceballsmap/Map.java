/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.spaceballsmap;

import java.awt.Image;

/**
 *
 * @author tkarpinski
 */
public abstract class Map {
    private Image image = null;

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

}
